import java.util.Random;
import org.junit.Test;

import static junit.framework.TestCase.assertNull;
import static org.junit.Assert.assertNotNull;
/**
 * Created by Артур on 10.12.2016.
 */
public class matrixtest {
    private int[][] massA = new int[10][10];
    private int size;

    public matrixtest() {
        this.size = 5;
        Random rand = new Random();
        for (int i = 0; i < (5); i++) {
            for (int j = 0; j < (5); j++) {
                massA[i][j] = rand.nextInt(50);
            }
        }
    }

    public matrixtest(int size) {
        this.size = size;
        Random rand = new Random();
        for (int i = 0; i < (size); i++) {
            for (int j = 0; j < (size); j++) {
                massA[i][j] = rand.nextInt(50);
            }
        }
    }

    public void print() {
        for (int i = 0; i < (this.size); i++) {
            for (int j = 0; j < (this.size); j++) {
                System.out.print(this.massA[i][j] + "\t");
            }
            System.out.println();
        }
    }
    @Test
    public void findmin() {

        int minimum = 100;
        int minrow = 0;
        int mincol = 0;
        int countofmin = 1;
        for (int i = 0; i < (this.size); i++) {
            for (int j = 0; j < (this.size); j++) {
                if (this.massA[i][j] == minimum) {
                    countofmin = countofmin + 1;
                }
                if (this.massA[i][j] < minimum) {
                    assertNull(massA[i][j]);
                    minimum = this.massA[i][j];
                    minrow = i;
                    mincol = j;
                }
            }

        }
        System.out.println("Наименьшее число массива : " + minimum + ",кол-во минимумов " + countofmin + ", номер строки : " + minrow + ", номер столбца : " + mincol);
    }
}

