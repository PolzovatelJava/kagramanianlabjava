package com.nmu.kagramanian.labrab4;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * Created by Артур on 10.12.2016.
 */
    public class matrix2 {
    private List<ArrayList<Integer>> massA= new ArrayList<ArrayList<Integer>>();
    private int size;
    public matrix2(){
        this.size=5;
        for (int j = 0; j < (5); j++) {
            this.massA.add(MakeList(size));
        }
    }

    private static ArrayList<Integer> MakeList(int size){
        ArrayList<Integer> promez=new ArrayList<Integer>();
        Random rand = new Random();
        for (int k = 0; k < (size); k++) {
            promez.add(rand.nextInt(20));
        }
        return promez;
    }
    public matrix2(int size){
        this.size=size;
        for (int j = 0; j < (size); j++) {
            this.massA.add(MakeList(size));
        }
    }
    public void print(){
        Integer i=-1;
        for (ArrayList<Integer> vnut: this.massA) {
            for (Integer integ: vnut) {
                i++;
                if (i == (this.size - 1)) {
                    System.out.println(integ);
                } else
                    System.out.print(integ + "\t");
            }
            i=-1;
        }
    }
    public void findkmin(){
        Integer minim=100;
        Integer i=1;
        for (ArrayList<Integer> vnut: this.massA) {
            Integer min= Collections.min(vnut);
            if (minim==min){
                i++;
            }
            if (minim>min){
                minim=min;
                i=1;
            }
        }

        System.out.println("Общее кол-во минимумов : " + i);
        System.out.println("Наименьшее число массива : " + minim);
    }

    public void findkmax() {
        Integer maxim=-1;
        Integer i=1;
        for (ArrayList<Integer> vnut: this.massA) {
            Integer max= Collections.max(vnut);
            if (maxim==max){
                i++;
            }
            if (maxim<max){
                maxim=max;
                i=1;
            }
        }
        System.out.println("Общее кол-во максимумов : " + i);
        System.out.println("Наибольшее число массива : " + maxim);
    }
}


