package com.nmu.kagramanian.labrab2;
import java.util.Random;
/**
 * Created by Артур on 10.12.2016.
 */
public class matrix {
    private int [][] massA= new int[4][4];
    private int size;
    public matrix(){
        this.size=5;
        Random rand = new Random();
        for (int i = 0; i < (5); i++) {
            for (int j = 0; j < (5); j++) {
                massA[i][j] = rand.nextInt(50);
            }
        }
    }
    public matrix(int size){
        this.size=size;
        Random rand = new Random();
        for (int i = 0; i < (size); i++) {
            for (int j = 0; j < (size); j++) {
                massA[i][j] = rand.nextInt(50);
            }
        }
    }
    public void print(){
        for (int i = 0; i < (this.size); i++) {
            for (int j = 0; j < (this.size); j++) {
                System.out.print(this.massA[i][j] + "\t");
            }
            System.out.println();
        }
    }
    public void findmin(){
        int minimum = 100;
        int countofmin=1;
        for (int i = 0; i < (this.size); i++) {
            for (int j = 0; j < (this.size); j++) {
                if (this.massA[i][j] == minimum) {
                    countofmin = countofmin + 1;
                }
                if (this.massA[i][j] < minimum) {
                    minimum = this.massA[i][j];
                    countofmin=1;
                }
            }

        }
        System.out.println("min element: " + minimum + ",kol-vo min elementov"+countofmin);
    }
}


