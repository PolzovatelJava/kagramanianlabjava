package com.nmu.kagramanian.labrab6;

/**
 * Created by Артур on 10.12.2016.
 */
public class Autosalon {
    String name;
    String country;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Autosalon autosalon = (Autosalon) o;

        if (!name.equals(autosalon.name)) return false;
        return name.equals(autosalon.name);
    }

}