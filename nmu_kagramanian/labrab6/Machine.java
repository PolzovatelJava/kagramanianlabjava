package com.nmu.kagramanian.labrab6;

/**
 * Created by Артур on 10.12.2016.
 */
public class Machine {
    String brand;
    String model;
    Integer price;
    Autosalon autosalon;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Machine mashina = (Machine) o;

        if (!brand.equals(mashina.brand)) return false;
        if (!model.equals(mashina.model)) return false;
        if (!price.equals(mashina.price)) return false;
        return autosalon.equals(mashina.autosalon);
    }


    @Override
    public int hashCode() {
        int result = brand.hashCode();
        result = 31 * result + model.hashCode();
        result = 31 * result + price.hashCode();
        return result;
    }
}

