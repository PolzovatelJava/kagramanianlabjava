package com.nmu.kagramanian.labrab6;
import com.google.gson.Gson;
/**
 * Created by Артур on 10.12.2016.
 */
public class Lab6 {
    public static void main(String[] args) {
        Machine mashina= new Machine();
        mashina.brand="BMW";
        mashina.model="x5";
        mashina.price= 54000;
        Autosalon autosalon=new Autosalon();
        autosalon.name="AutoRay";
        autosalon.country="Ukraine";
        mashina.autosalon=autosalon;
        Gson gson=new Gson();
        String json=gson.toJson(mashina);
        System.out.println(json);
        Machine studentFromJson= new Machine();
        gson.fromJson(json,Machine.class);
    }

}
