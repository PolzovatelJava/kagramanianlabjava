package com.nmu.kagramanian;
import java.util.Random;
/**
 * Created by Артур on 10.12.2016.
 */
public class Lab1 {
    public static void main(String[] args) {
        System.out.println("Поиск максимального элемента в массиве");
        int[]mass= new int[15];
        Random rnd= new Random();

        for(int i=0; i<mass.length;i++)
            mass[i]=rnd.nextInt(100);
        for(int i=0; i<mass.length;i++)
            System.out.print(mass[i]+" ");
        int maxIndex = 0;
        for (int i = 0; i < mass.length; i++)
        {
            if (mass[maxIndex] < mass[i])
                maxIndex = i;}
        System.out.println(" Максимальный эллемент = " + mass[maxIndex]);

        System.out.println("Поиск максимального элемента в матрице");
        int[][] matrixA;
        matrixA = new int[5][5];
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                matrixA[i][j]=rnd.nextInt(100);
            }
            System.out.println();
        }
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                System.out.print(matrixA[i][j] + "\t");
            }
            System.out.println();
        }
        int maxElement=0;
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                if (maxElement < matrixA[i][j])
                    maxElement = matrixA[i][j];
            }
        }
        System.out.println(" Максимальный эллемент = " + maxElement);
    }
}


